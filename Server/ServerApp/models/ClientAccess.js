const { CLIENT_ACCESS } = require('../constantsModel');

module.exports = (sequelize, DataType) => {
    const ClientAccess = sequelize.define(CLIENT_ACCESS, {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        apiKey: {
            type: DataType.STRING,
            allowNull: false
        },
        ip: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                len: [0,32],
            }
        },
        userName: {
            type: DataType.STRING,
            allowNull: false
        },
        password: {
            type: DataType.STRING,
            allowNull: false
        },
        registerDate: {
            type: DataType.DATE,
            defaultValue: DataType.NOW
        }
    });

    return ClientAccess;
}

module.exports.ClientAccess = class ClientAccess {
    constructor(id, apiKey, ip, userName, password, registerDate){
        this._id = id;
        this._apiKey = apiKey;
        this._userName = userName;
        this._password = password;
        this._registerDate = registerDate;
    }

    get JSON() {
        return {
            apiKey: this._apiKey,
            userName: this._userName,
            password: this._password
        }
    }
}
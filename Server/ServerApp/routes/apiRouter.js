let express = require('express');
let router = express.Router();
const { CLIENT_ACCESS } = require('../constantsModel');
const RegisterController = require('../apiControllers/RegisterController');

var dbSource = null;
let registerController = null;


router.route('/user/reg').get((req, res) => {
    registerController.acceptUser(req, res);
});

module.exports = (db) => {
    dbSource = db;
    registerController = new RegisterController(db);
    return router 
};

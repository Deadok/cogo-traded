const { CLIENT_ACCESS } = require('../ModelConstants');
const { Model } = require('sequelize');


/**
 * 
 * 
 * @class ApiControllerBase
 */
//@ts-check
class ApiControllerBase {
    /**
     * Creates an instance of ApiControllerBase.
     * @param {any} db 
     * 
     * @memberof ApiControllerBase
     */
    constructor(db) {
        this._db = db;
    }

    /**
     * 
     * 
     * @readonly
     * @returns {Model}
     * @memberOf ApiControllerBase
     */
    get ClientAccess() {
        return this._db.models[CLIENT_ACCESS];
    }
}

module.exports = ApiControllerBase;
'use strict'

const config = require('../configuration');
const Sequelize = require('sequelize');
let sequelize = new Sequelize(config.dbConnection);
const path = require('path');
const fs = require('fs');
//Models
let database = null; 

function loadModels(sequelize) {
    let modelsDir = path.join(__dirname, 'models');
    let models = {};
    Array.from(fs.readdirSync(modelsDir), file => {
        const modelPath = path.join(modelsDir, file);
        const model = sequelize.import(modelPath);
        models[model.name] = model;
    });

    return models;
}

module.exports = () => {
    if(!database) {
        database = {
            sequlize: sequelize,
            Sequelize: Sequelize,
            models: null
        };

        database.models = loadModels(sequelize);
        sequelize.sync().then(() => {
            return database;
        });
    }

    return database;
}

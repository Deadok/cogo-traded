'use strict'
let express = require('express');
let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');

let defPort = 9100;
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())
app.set("port", process.env.port || defPort);
let dbSource = require('./ServerApp/dbsource')();
//Create router
let apiRouter = require('./ServerApp/routes/apiRouter')(dbSource);
app.use('/api/v1', apiRouter);

app.use(express.static(__dirname + '/public'));
app.listen(app.get("port"), () => {
    console.log("APP LITENING AT PORT: " + app.get("port"));
});







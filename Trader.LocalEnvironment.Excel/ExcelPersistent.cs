﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Objects.Portable.Objects;
using OfficeOpenXml;

namespace Trader.LocalEnvironment.Excel
{
    public class ExcelPersistent
    {

        private string _filePath;

        private string _folderPath;

        private Marketplace _marketplace;

        private int _rowCounter = 1;

        private FileInfo _fi;

        private void InitFileHeaders(FileStream file)
        {
            using (var xlPackage = new ExcelPackage(file))
            {
                // do work here
                var workSheet = xlPackage.Workbook.Worksheets[_marketplace.Name];
                if (workSheet == null)
                {
                    workSheet = xlPackage.Workbook.Worksheets.Add(_marketplace.Name);
                }

                var range = workSheet.Cells[1, 1, 1, 9];
                range.Style.Font.Bold = true;
                range.Style.Font.Size = 14;
                workSheet.Cells[_rowCounter, 1].Value = "№";
                workSheet.Column(1).Width = 2;
                workSheet.Cells[_rowCounter, 2].Value = "Предмет";
                workSheet.Column(2).Width = 10;
                workSheet.Cells[_rowCounter, 3].Value = "Steam ID";
                workSheet.Column(3).Width = 10;
                workSheet.Cells[_rowCounter, 4].Value = "Кейс";
                workSheet.Cells[_rowCounter, 5].Value = "Цена предмета";
                workSheet.Cells[_rowCounter, 6].Value = "Цена кейса";
                workSheet.Cells[_rowCounter, 7].Value = "Пользователь";
                workSheet.Cells[_rowCounter, 8].Value = "Ссылка";
                workSheet.Cells[_rowCounter, 9].Value = "Дата получения";
                workSheet.Column(9).Width = 10;
                //workSheet.
                var start = workSheet.Dimension.Start;
                var end = workSheet.Dimension.End;
                for (int row = start.Row; row <= end.Row; row++)
                {
                    if (!String.IsNullOrEmpty(workSheet.Cells[row, 1].Text))
                    {
                        _rowCounter++;
                    }
                }

                xlPackage.Save();
            }
        }

        public void StoreExtractionData(IEnumerable<ContainerExtractionData> edta)
        {
            using (var xlPackage = new ExcelPackage(_fi))
            {
                var workSheet = xlPackage.Workbook.Worksheets[_marketplace.Name];
                foreach (var ced in edta)
                {
                    workSheet.Cells[_rowCounter, 1].Value = ced.Id;
                    workSheet.Cells[_rowCounter, 2].Value = ced.Item.Name;
                    workSheet.Cells[_rowCounter, 3].Value = ced.Item.SteamId;
                    workSheet.Cells[_rowCounter, 4].Value = ced.Container.Name;
                    workSheet.Cells[_rowCounter, 5].Value = ced.Item.MarketplacePrice.Price;
                    workSheet.Cells[_rowCounter, 6].Value = ced.Container.Price;
                    workSheet.Cells[_rowCounter, 7].Value = ced.User.Name;
                    workSheet.Cells[_rowCounter, 8].Value = ced.UserUrl;
                    workSheet.Cells[_rowCounter, 9].Value = ced.DateRecive.ToString("dd.MM.yyyy HH:mm:ss");
                    _rowCounter++;
                }

                xlPackage.Save();
            }
        }

        public void Init()
        {
            if (!Directory.Exists(_folderPath))
            {
                Directory.CreateDirectory(_folderPath);
            }

            if (!File.Exists(_filePath))
            {
                using (var file = File.Create(_filePath))
                {
                    InitFileHeaders(file);
                }
            }
            else
            {
                using (var file = File.OpenRead(_filePath))
                {
                    InitFileHeaders(file);
                }
            }

            _fi = new FileInfo(_filePath);
        }

        public ExcelPersistent(string excelFolder, string fileName, Marketplace marketplace)
        {
            _folderPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), excelFolder));
            _filePath = Path.Combine(_folderPath, fileName);
            _marketplace = marketplace;
        }
    }
}

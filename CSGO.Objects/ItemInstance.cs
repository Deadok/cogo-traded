﻿namespace Trader.Objects
{
    public class ItemInstance
    {
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        /// Идентификатор экземпляра на торговой площадке
        /// </summary>
        public int InstanceId { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Количество предложений
        /// </summary>
        public int Offers { get; set; }

        /// <summary>
        /// Редкость
        /// </summary>
        public ERarity Rarity { get; set; }

        /// <summary>
        /// Качество
        /// </summary>
        public EQuality Quality { get; set; }

    }
}

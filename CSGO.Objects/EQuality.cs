﻿using System.ComponentModel;

namespace Trader.Objects
{
    public enum EQuality
    {
        [Description("Немного поношенное")]
        MinimalWear = 0,
        [Description("Закаленное в боях")]
        BattleScarred = 1,
        [Description("Прямо с завода")]
        FactoryNew = 2,
        [Description("Поношенное")]
        WellWorn = 3,
        [Description("После полевых испытаний")]
        FieldTested = 4,
        [Description("Не покрашено")]
        NotPainted = 5,
    }
}

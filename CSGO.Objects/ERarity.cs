﻿using System.ComponentModel;

namespace Trader.Objects
{
    public enum ERarity
    {
        [Description("Базового класса")]
        ConsumerGrade = 0,
        [Description("Ширпотреб")]
        Casual = 1,
        [Description("Промышленное качество")]
        IndustrialGrade = 2,
        [Description("Армейское качество")]
        MilSpec = 3,
        [Description("Запрещенное")]
        Restricted = 4,
        [Description("Засекреченное")]
        Classified = 5,
        [Description("Тайное")]
        Covert = 6,
        [Description("Высшего класса")]
        HiClass = 7,
        [Description("Примечательного типа")]
        Rare = 8,
        [Description("Экзотичного вида")]
        Exotic = 9,
        [Description("Контрабандное")]
        Smuggling = 10,
        [Description("Экстраординарного типа")]
        Extraordinary = 11
    }
}

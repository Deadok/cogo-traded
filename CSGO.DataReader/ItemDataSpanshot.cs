﻿using System;
using System.Collections.Generic;
using Trader.Objects;

namespace Trader.Plugin
{
    public class ItemDataSpanshot
    {
        /// <summary>
        /// Дата снапшота
        /// </summary>
        public DateTime SpanshotTime { get; set; }

        /// <summary>
        /// Продаваемые предметы
        /// </summary>
        public List<ItemInstance> ItemInstances { get; set; } 
    }
}

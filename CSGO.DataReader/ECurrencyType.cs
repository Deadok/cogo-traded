﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trader.Plugin
{
    public enum ECurrencyType
    {
        [Description("долл.")]
        Usd = 1,
        [Description("Руб.")]
        Rub = 5,
        [Description("Евр.")]
        Eur = 3
    }
}

﻿namespace Trader.Plugin.PluginApi
{
    public class Credentials
    {
        public Credentials(string password, string user, string apiKey, EmarketCredentialType credentialType)
        {
            Password = password;
            User = user;
            ApiKey = apiKey;
            CredentialType = credentialType;
        }

        public string Password { get; set; }

        public string User { get; set; }

        public string ApiKey { get; set; }

        public EmarketCredentialType CredentialType { get; private set; }
    }
}

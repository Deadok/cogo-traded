﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Trader.Objects;

namespace Trader.Plugin.PluginApi
{
    public abstract class MarketPlacePlugin
    {
        /// <summary>
        /// Configure plugin
        /// </summary>
        /// <param name="credentials"> Данные авторизации в API торговой площадки </param>
        public abstract void SetUp(Credentials credentials);

        /// <summary>
        /// Get money balance on Marketplace
        /// </summary>
        /// <returns></returns>
        public abstract double GetBalance();

        /// <summary>
        /// Попытаться получить новый снапшот
        /// </summary>
        /// <param name="snap"> Снапшот продаваемых объектов </param>
        /// <returns></returns>
        public abstract bool LoadItemsSnapshot(out ItemDataSpanshot snap);

        /// <summary>
        /// Begin plugin updating (background)
        /// </summary>
        public abstract void StartPlugin();

        /// <summary>
        /// Stop plugin updating (background)
        /// </summary>
        public abstract void StopPlugin();

        /// <summary>
        /// Initialize internal plugin data (peforms async)
        /// </summary>
        /// <param name="env"></param>
        public abstract void InitPlugin(PluginEnvironment env);

        /// <summary>
        /// Get plugin settings view model
        /// </summary>
        public abstract ISettings PluginSettings { get; }

        /// <summary>
        /// Список айтемов в инвентарии на торговой площадке
        /// </summary>
        public abstract List<ItemInstance> MarketplaceInventory {get;}

        /// <summary>
        /// Analytics form interface
        /// </summary>
        public abstract IAnalytics GetPluginAnalytics { get; }

        public abstract UserControl PluginUi { get; }

        /// <summary>
        /// Initialize plugin interface
        /// Calls in UI thread
        /// Create PluginUI, Plugin settings instances here
        /// </summary>
        public abstract void InitInterface();

        public virtual string PluginName { get; private set; }

        public void SetName(string name)
        {
            PluginName = name;
        }

        public Guid PluginGuid { get; private set; }

        public abstract Visual IconVisual { get; }

        public event EventHandler<MarketPlacePlugin> PluginReadyForUse;

        protected void NotifyReadyForUse()
        {
            if (PluginReadyForUse != null)
            {
                PluginReadyForUse.Invoke(this, this);
            }
        }

        public void SetGuid(Guid guid)
        {
            PluginGuid = guid;
        }
    }
}

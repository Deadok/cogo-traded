﻿using System.Globalization;
using System.Windows.Controls;

namespace Trader.Plugin.PluginApi
{
    public interface ISettings
    {
        /// <summary>
        /// Settings owner
        /// </summary>
        MarketPlacePlugin Owner { get; }

        /// <summary>
        /// Plugin name for settings window
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Plugin display control
        /// </summary>
        UserControl DisplayControl { get; }

        /// <summary>
        /// Save plugin settings
        /// </summary>
        void SaveSettings();

        /// <summary>
        /// Reset default setrings
        /// </summary>
        void ResetDefault();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trader.Plugin
{
    /// <summary>
    /// Plugin Environment (general settings and options)
    /// </summary>
    public class PluginEnvironment
    {
        public PluginEnvironment(CultureInfo cultureInfo)
        {
            CultureInfo = cultureInfo;
        }

        public CultureInfo CultureInfo { get; private set; }

        public void SetCulture(CultureInfo info)
        {
            CultureInfo = info;
            if (CultureChanged != null)
            {
                CultureChanged.Invoke(this, info);
            }
        }

        public event EventHandler<CultureInfo> CultureChanged;
    }
}

﻿using System;
using System.Security;
using CSGO.Trader.SecureStore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;

namespace Trader.Tests
{
    [TestClass]
    public class PasswordStorageTest
    {
        public PasswordStoreSecure _secureStorage;
        public Guid TestGuid;
        private const string TestableString = "tstr";

        public PasswordStorageTest()
        {
            _secureStorage = new PasswordStoreSecure();
            TestGuid = new Guid(1,1,1,1,0,1,1,0,1,0,1);
        }

        [TestMethod]
        public void TestPasswordProtect()
        {
            var secStr = _secureStorage.StoreSteamPasswordSecure(TestableString, TestGuid);
            Assert.AreEqual(_secureStorage.UnsecureString(secStr), TestableString);
;       }

        [TestMethod]
        public void TestPasswordStoreAndLoad()
        {
            _secureStorage.StoreSteamPasswordSecure(TestableString, TestGuid);
            SecureString str;
            if (_secureStorage.TryGetPluginPassword(out str, TestGuid))
            {
                Assert.AreEqual(_secureStorage.UnsecureString(str), TestableString);
            }
            else
            {
                Assert.Fail("Can'not retrieve password");
            }
        }
    }
}

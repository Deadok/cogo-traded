﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects.Portable
{
    /// <summary>
    /// Тип торговой площадки
    /// </summary>
    [Flags]
    public enum MarketplaceType : int
    {
        /// <summary>
        /// Продажа и открытие контейнеров со скинами
        /// </summary>
        ContainersMarket = 0x01,

        /// <summary>
        /// Торговля предметами
        /// </summary>
        ItemTradeMarket = 0x02,

        /// <summary>
        /// Торговля контейнерами и предметами
        /// </summary>
        ContainersAndTrade = ItemTradeMarket | ContainersMarket
    }
}

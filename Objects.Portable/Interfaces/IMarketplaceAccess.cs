﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Objects.Portable.Objects;

namespace Objects.Portable.Interfaces
{
    interface IMarketplaceAccess
    {
        /// <summary>
        /// Получить площадку по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Marketplace GetMarketplaceById(int id);

        /// <summary>
        /// Получить площадку по имени
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Marketplace GetMarketplaceByName(string id);

        Marketplace[] GetAll();
    }
}

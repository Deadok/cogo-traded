﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Objects.Portable.Objects;

namespace Objects.Portable.Interfaces
{
    interface IContainersAccess
    {
        /// <summary>
        /// Получить контейнер по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Container GetContainer(int id);

        /// <summary>
        /// Получить все контейнеры
        /// </summary>
        /// <returns></returns>
        Container[] GetContainers();

        /// <summary>
        /// Получить контейнеры конкретной площадки
        /// </summary>
        /// <param name="marketpalce"></param>
        /// <returns></returns>
        Container[] GetMarketplaceContainers(Marketplace marketpalce);
    }
}

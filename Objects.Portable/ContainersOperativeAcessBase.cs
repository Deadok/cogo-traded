﻿using System;
using System.Linq;
using System.Threading;
using Objects.Portable.Objects;

namespace Objects.Portable
{
    public delegate void ExtractedOperativeDelegate(ContainerExtractionData[] extractionData);

    /// <summary>
    /// Предоставляет доступ к оперативным данным открытия конейтеров площадки
    /// </summary>
    public abstract class ContainersOperativeAcessBase
    {
        /// <summary>
        /// Имя сайта из которого извлекается информация
        /// </summary>
        public string ResourceName { get; private set; }
        
        /// <summary>
        /// УРЛ сайта
        /// </summary>
        public string Url { get; private set; }

        public Marketplace Marketplace { get; private set; }

        protected ContainersOperativeAcessBase(Marketplace marketplace)
        {
            Marketplace = marketplace;
        }

        /// <summary>
        /// Запустить процесс извлечения данных об открытии кейсов из ресурка
        /// </summary>
        /// <param name="arg"></param>
        public abstract void StartDataExtraction(CancellationToken arg);

        protected virtual void NotifyDataExtracted(ContainerExtractionData[] newData)
        {
            if (DataExtracted != null)
            {
                try
                {
                    if (newData.Any())
                    {
                        DataExtracted.Invoke(newData);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Ошибка при уведомлении о новых данных {Marketplace.Name} - {Marketplace.Url}", ex);
                }
            }
        }

        /// <summary>
        /// Уведомляет о наличии новых извлеченных данных
        /// </summary>
        public event ExtractedOperativeDelegate DataExtracted;

    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Objects.Portable.Objects
{
    /// <summary>
    /// Описание контейнера из которого выпал предмет
    /// </summary>
    [Table("[trader].[CONTAINERS]")]
    public class Container
    {
        /// <summary>
        /// Идентификатор контейнера
        /// </summary>
        public int ContainerKey { get; set; }

        /// <summary>
        /// Имя контейнера
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Площадка-эмитет контейнера
        /// </summary>
        public int MarketplaceKey { get; set; }

        public Marketplace Marketplace { get; set; }
    }
}

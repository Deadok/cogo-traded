﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Objects.Portable.Objects
{
    [Table("[trader].[MARKETPLACE_PRICES]")]
    public class ItemMarketplacePrice
    {
        public long Id { get; set; }

        public double Price { get; set; }

        public string ItemKey { get; set; }

        public int MarketplaceKey { get; set; }

        public Item Item { get; set; }

        public Marketplace Marketplace { get; set; }
    }
}

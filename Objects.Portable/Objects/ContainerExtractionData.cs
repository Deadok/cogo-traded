﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Objects.Portable.Objects
{
    /// <summary>
    /// Содержит информцию об открытии контейнера
    /// </summary>
    [Table("[trader].[OPERATIVE_DATA]")]
    public class ContainerExtractionData
    {
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// ID площадки-владельца
        /// </summary>
        public int MarketplaceKey { get; set; }

        /// <summary>
        /// Площадка
        /// </summary>
        public Marketplace Marketplace { get; set; }

        /// <summary>
        /// Дата получения
        /// </summary>
        public DateTime DateRecive { get; set; }

        /// <summary>
        /// Идентификатор пользователя получившего контейнер
        /// </summary>
        public int UserKey { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Идентификатор предмета в системе
        /// </summary>
        public string ItemKey { get; set; }


        /// <summary>
        /// Информация о предмете
        /// </summary>
        public Item Item { get; set; }

        /// <summary>
        /// Идентификатор контейнера в системе
        /// </summary>
        public int ContainerKey{ get; set; }

        /// <summary>
        /// Ссылка на контейнер
        /// </summary>
        public Container Container { get; set; }

        /// <summary>
        /// Ссылка на профиль пользователя если есть доступ
        /// </summary>
        public string UserUrl { get { return Marketplace?.Url + User.UserId; } }
    }
}

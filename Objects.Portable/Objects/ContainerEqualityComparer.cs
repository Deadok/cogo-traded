﻿using System.Collections.Generic;

namespace Objects.Portable.Objects
{
    /// <summary>
    /// Проверка эквивалентности двух открытий контейнера
    /// </summary>
    public class ContainerEqualityComparer : IEqualityComparer<ContainerExtractionData>
    {
        public bool Equals(ContainerExtractionData x, ContainerExtractionData y)
        {
            if (x == null || y == null)
            {
                return false;
            }

            return x.Item.SteamId == y.Item.SteamId && x.User.UserId == y.User.UserId && x.Container.Name == y.Container.Name && x.DateRecive == y.DateRecive;

        }

        public int GetHashCode(ContainerExtractionData obj)
        {
            return obj.DateRecive.GetHashCode() ^ obj.Container.Name.GetHashCode() ^ obj.User.UserId.GetHashCode() ^ obj.Item.SteamId.GetHashCode();
        }
    }
}

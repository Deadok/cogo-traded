﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Objects.Portable.Objects
{
    /// <summary>
    /// Информация о пользователе, открывшем контейнер
    /// </summary>
    [Table("[trader].[USERS]")]
    public class User
    {
        public User()
        {
            //SteamId = int.Parse(dict["id"].ToString());
            //Name = dict["name"].ToString();
        }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отношение плользователя к площадке
        /// </summary>
        public int MarkeyplaceKey { get; set; }

        public Marketplace Marketplace { get; set; }
    }
}

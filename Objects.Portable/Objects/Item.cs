﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Objects.Portable.Objects
{
    /// <summary>
    /// Информация об извлеченном из контейнера предмете
    /// </summary>
    [Table("[trader].[ITEMS]")]
    public class Item
    {
        public Item()
        {
            //SteamId = dict["itemID"].ToString();
            //Name = $"{dict["displayFirstName"]} {dict["displaySecondName"]}";
            //ItemPrice = double.Parse(dict["itemPrice"].ToString());
            //Price = double.Parse(dict["price"].ToString());
            //Rarity = dict["rarity"].ToString();
        }

        /// <summary>
        /// Идентификатор предмета в нашей системе
        /// </summary>
        public string ItemId { get; set; }

        /// <summary>
        /// Имя предмета
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Цена предмета
        /// </summary>
        public long ItemMarketplacePriceId { get; set; }

        /// <summary>
        /// Ссылка на ценник в конкретном магазине
        /// </summary>
        public ItemMarketplacePrice MarketplacePrice { get; set; }

        /// <summary>
        /// Идентификатор предмета в стим
        /// </summary>
        public string SteamId { get; set; }

        /// <summary>
        /// Ценность предмета
        /// </summary>
        public string Rarity { get; set; }
    }
}

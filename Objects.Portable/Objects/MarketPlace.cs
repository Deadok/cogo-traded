﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects.Portable.Objects
{
    /// <summary>
    /// Площадка-владелец операции открытия кейса
    /// </summary>
    [Table("[trader].[MARKETPLACES]")]
    public class Marketplace
    {
        public Marketplace()
        {
            
        }

        public int MarketplaceId { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string UserProfilePath { get; set; }

        public string ContainersDescriptionUrl { get; set; }

        public string UserProfileUrl { get; set; }

        public MarketplaceType TypeKey { get; set; }
    }
}

﻿using System;
using Xceed.Wpf.AvalonDock.Themes;

namespace CSGO.Trader.DockTheme
{
    public class AvalonTheme : Theme
    {
        public override Uri GetResourceUri()
        {
            return new Uri(
                "/CSGO.Trader;component/DockTheme/AvalonStylesDictionary.xaml",
                UriKind.Relative);
        }
    }
}
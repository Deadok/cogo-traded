﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace CSGO.Trader
{
    /// <summary>
    /// Interaction logic for UnhandledErrorWindow.xaml
    /// </summary>
    public partial class UnhandledErrorWindow : MetroWindow
    {
        public UnhandledErrorWindow(Exception ex, Window owner)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            this.Owner = owner;
            this.MainErrMessage.Text = ex.Message +"\n" + ex.InnerException?.Message;
            MainExText.AppendText(ex.ToString());
            SystemSounds.Hand.Play();
            this.Focus();
        }
    }
}

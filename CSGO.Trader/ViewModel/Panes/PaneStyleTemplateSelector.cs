﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using CSGO.Trader.PluginFunctional;

namespace CSGO.Trader.ViewModel.Panes
{
    public class PaneStyleTemplateSelector : StyleSelector
    {
        public Style MarketplacePluginStyle { get; set; } 

        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is PluginMarketplaceViewModel)
            {
                return MarketplacePluginStyle;
            }

            return base.SelectStyle(item, container);
        }
    }
}

﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using CSGO.Trader.ViewModel.Panes;
using GalaSoft.MvvmLight.CommandWpf;
using Trader.Plugin.PluginApi;

namespace CSGO.Trader.ViewModel
{
    public delegate void PluginClosingDelegate(PluginBaseViewModel vm);

    public abstract class PluginBaseViewModel : PaneViewModel
    {
        public PluginBaseViewModel(MarketPlacePlugin plugin, UserControl mainInterface)
        {
            Title = plugin.PluginName;
            PluginId = plugin.PluginGuid;
            Plugin = plugin;
            _pluginViewInstance = mainInterface;
        }

        public Guid PluginId { get; private set; }

        public MarketPlacePlugin Plugin { get; private set; }

        public UserControl PluginViewInstance
        {
            get { return _pluginViewInstance; }
            set
            {
                _pluginViewInstance = value;
                RaisePropertyChanged(() => PluginViewInstance);
            }
        }

        private UserControl _pluginViewInstance;
        private bool _isInitializing;
        private bool _isNotInitializing;

        /// <summary>
        /// Плагин инициализируется.
        /// </summary>
        public bool IsInitializing
        {
            get { return _isInitializing; }
            set
            {
                _isInitializing = value;
                IsNotInitializing = !value;
                RaisePropertyChanged(() => IsInitializing);
            }
        }

        /// <summary>
        /// Плагин инициализируется.
        /// </summary>
        public bool IsNotInitializing
        {
            get { return _isNotInitializing; }
            set
            {
                _isNotInitializing = value;
                RaisePropertyChanged(() => IsNotInitializing);
            }
        }

        protected void InvokeClosing()
        {
            if (PluginClosing != null)
            {
                PluginClosing.Invoke(this);
            }
        }

        public event PluginClosingDelegate PluginClosing;

        public ICommand CloseCommand { get; set; }
    }
}

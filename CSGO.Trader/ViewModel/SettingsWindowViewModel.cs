﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using CSGO.Trader.PluginFunctional;
using CSGO.Trader.ViewModel.Settings;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NLog;
using Trader.Plugin.PluginApi;

namespace CSGO.Trader.ViewModel
{
    public class SettingsWindowViewModel : ViewModelBase
    {
        private ILogger _logger;

        /// <summary>
        /// Collection of lplugin settings
        /// </summary>
        private BindingList<ISettings> _pluginSettingsCollection;

        /// <summary>
        /// Selected plugin settings tab
        /// </summary>
        private ISettings _selectedSettingsTab;

        /// <summary>
        /// Store command
        /// </summary>
        public ICommand StoreCommand { get; set; }

        /// <summary>
        /// Revert all changes without saving
        /// </summary>
        public ICommand ResetSettingsCommand { get; set; }

        public SettingsWindowViewModel()
        {
            PluginManager.Instance.PluginCollectionChanged += InstanceOnPluginCollectionChanged;
            _logger = LogManager.GetCurrentClassLogger();
            _pluginSettingsCollection = new BindingList<ISettings>();
            StoreCommand = new RelayCommand(() =>
            {
                foreach (var settings in _pluginSettingsCollection)
                {
                    try
                    {
                        settings.SaveSettings();
                    }
                    catch (Exception ex)
                    {
                        Helpers.LogAndRethrow(_logger, $"Error during saving plugin settings: {settings?.Owner?.PluginName}", ex);
                    }
                }
            });

            ResetSettingsCommand = new RelayCommand(() =>
            {
                foreach (var settings in _pluginSettingsCollection)
                {
                    try
                    {
                        settings.ResetDefault();
                    }
                    catch (Exception ex)
                    {
                        Helpers.LogAndRethrow(_logger, $"Error during reset plugin settings: {settings?.Owner?.PluginName}", ex);
                    }
                }
            });
        }

        private void InstanceOnPluginCollectionChanged(MarketPlacePlugin[] collection)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                PluginSettingsCollection.Clear();
                //Add general application settrings
                PluginSettingsCollection.Add(new GeneralSettingsViewModel());
                //Load 
                foreach (var marketPlacePlugin in collection)
                {
                    try
                    {
                        PluginSettingsCollection.Add(marketPlacePlugin.PluginSettings);
                    }
                    catch (Exception ex)
                    {
                        Helpers.LogAndRethrow(_logger, "Error during initialize plugin settings", ex);
                    }
                }

                PluginSettingsCollection.ResetBindings();
            }));
        }

        public override void Cleanup()
        {
            base.Cleanup();
            PluginManager.Instance.PluginCollectionChanged -= InstanceOnPluginCollectionChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        public ISettings SelectedSettingsTab
        {
            get { return _selectedSettingsTab; }
            set
            {
                _selectedSettingsTab = value;
                RaisePropertyChanged(() => SelectedSettingsTab);
            }
        }

        public BindingList<ISettings> PluginSettingsCollection
        {
            get { return _pluginSettingsCollection; }
            set { _pluginSettingsCollection = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace CSGO.Trader
{
    public static class Helpers
    {
        public static void LogAndRethrow(ILogger logger, string text, Exception ex)
        {
            var newEx = new Exception(text, ex);
            logger.Error(newEx);
            throw newEx;
        }
    }
}

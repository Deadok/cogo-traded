﻿using System.Threading;
using CefSharp.OffScreen;

namespace CSGO.Trader.IntegratedPlugins.TopSkinMarketpace.Logic.Support
{
    public class StartExtractionEventArgs
    {
        public CancellationToken Cts { get; private set; }

        public ChromiumWebBrowser Browser { get; private set; }

        public StartExtractionEventArgs(CancellationToken cts, ChromiumWebBrowser browser)
        {
            Cts = cts;
            Browser = browser;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CefSharp;

namespace CSGO.Trader.IntegratedPlugins.TopSkinMarketpace.Logic
{
    public class RenderProcessMessageHandler : IRenderProcessMessageHandler
    {
        private ManualResetEvent _mrs;

        public RenderProcessMessageHandler(ManualResetEvent mrs)
        {
            _mrs = mrs;
        }

        // Wait for the underlying JavaScript Context to be created. This is only called for the main frame.
        // If the page has no JavaScript, no context will be created.
        void IRenderProcessMessageHandler.OnContextCreated(IWebBrowser browserControl, IBrowser browser, IFrame frame)
        {
            _mrs.Set();
        }

        public void OnContextReleased(IWebBrowser browserControl, IBrowser browser, IFrame frame)
        {
            return;
        }

        public void OnFocusedNodeChanged(IWebBrowser browserControl, IBrowser browser, IFrame frame, IDomNode node)
        {
            return;
        }
    }
}

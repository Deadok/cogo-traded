﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.OffScreen;
using CSGO.Trader.IntegratedPlugins.TopSkinMarketpace.Logic.Support;
using Objects.Portable;
using Objects.Portable.Objects;

namespace CSGO.Trader.IntegratedPlugins.TopSkinMarketpace.Logic
{
    public class BrowserOperativeAccess : ContainersOperativeAcessBase
    {
        private ManualResetEvent _mrs;

        private ManualResetEvent _queueDataHalt = new ManualResetEvent(false);

        #region qry

        private const string DataQry = @"(function() 
                        { 
                            var res = [];
                            var body = window.document.getElementsByClassName('item-history');
                            for (var i = 0; i < body.length; i++) {
                                res.push(Template.currentData(body[i]));
                            };

                            return  res; 
                        })();";

        #endregion

        /// <summary>
        /// Ссылка на браузер для парсинга
        /// </summary>
        private ChromiumWebBrowser _browser;

        // private void Queue _concurrentBag;
        private ConcurrentBag<List<Dictionary<string, object>>> _concurrentBag;

        /// <summary>
        /// Тред в котором происходит извлечение
        /// </summary>
        private Thread _extractionThread;

        /// <summary>
        /// Тред в котором происходит обработка данных и сохранение в СУБД
        /// </summary>
        private Thread _dataProcessingThread;

        /// <summary>
        /// Токен отмены работы
        /// </summary>
        CancellationToken _ct;

        /// <summary>
        /// Выполняет преобразование в объектную модель, после извлечения данных из браузера
        /// </summary>
        private BrowserDataInterpreter _interpreter;

        /// <summary>
        /// Проброс ожидения загрузки
        /// </summary>
        public ManualResetEvent WaitInitialLoad { get { return _mrs; } }

        public BrowserOperativeAccess(Marketplace marketplace, ChromiumWebBrowser browser)
                : base(marketplace)
            {
            _mrs = new ManualResetEvent(false);
            _interpreter = new BrowserDataInterpreter();
            _browser = browser;
            _browser.RenderProcessMessageHandler = new RenderProcessMessageHandler(_mrs);
            _concurrentBag = new ConcurrentBag<List<Dictionary<string, object>>>();
        }

        private void ExtractionBackground(object o)
        {
            var ea = (StartExtractionEventArgs)o;
            while (true)
            {
                _mrs.WaitOne();
                if (_ct.IsCancellationRequested)
                {
                    _queueDataHalt.Set();
                    return;
                }

                var t = ea.Browser.EvaluateScriptAsync(DataQry);
                t.Wait();
                var res = t.Result.Result as List<object>;
                if (res != null)
                {
                    var data = res.Select(i => ((Dictionary<string, object>)i)).ToList();
                    _concurrentBag.Add(data);
                    _queueDataHalt.Set();
                }
            }
        }

        /// <summary>
        /// Преобразование данных и сохранение их в СУБД
        /// </summary>
        /// <param name="obj"></param>
        private void DataProcessing(object obj)
        {
            _mrs.WaitOne();
            var token = (CancellationToken)obj;
            long ids = 0;
            var uq = new ContainerEqualityComparer();
            ContainerExtractionData[] extracted = null;
            List<Dictionary<string, object>> objs;
            while (true)
            {
                _queueDataHalt.WaitOne();
                if (token.IsCancellationRequested)
                {
                    return;
                }

                if (_concurrentBag.TryTake(out objs))
                {
                    var interpreted = _interpreter.GetItems(objs);
                    if (extracted == null)
                    {
                        extracted = interpreted;
                        foreach (var itp in extracted)
                        {
                            itp.Id = ++ids;
                        }

                        NotifyDataExtracted(extracted);
                        continue;
                    }

                    var newItems = interpreted.Except(extracted, uq).ToArray();
                    extracted = interpreted;
                    foreach (var itp in newItems)
                    {
                        itp.Id = ++ids;
                    }

                    NotifyDataExtracted(newItems);
                }

                if (_concurrentBag.IsEmpty)
                {
                    _queueDataHalt.Reset();
                }
            }
        }

        /// <summary>
        /// Основной метод запуска извлечения данных из браузера
        /// </summary>
        /// <param name="arg"></param>
        public override void StartDataExtraction(CancellationToken arg)
        {
            _ct = arg;
            var ea = new StartExtractionEventArgs(_ct, _browser);
            _extractionThread = new Thread(ExtractionBackground);
            _dataProcessingThread = new Thread(DataProcessing);
            _extractionThread.Start(ea);
            _dataProcessingThread.Start(_ct);
        }
    }
}

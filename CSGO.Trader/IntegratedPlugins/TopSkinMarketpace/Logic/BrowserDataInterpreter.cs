﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Objects.Portable.Objects;

namespace CSGO.Trader.IntegratedPlugins.TopSkinMarketpace.Logic
{
    public class BrowserDataInterpreter
    {
        public ContainerExtractionData[] GetItems(List<Dictionary<string, object>> items)
        {
            var res = new List<ContainerExtractionData>();
            foreach (var extractedItem in items)
            {
                var item = new Item();
                var itemData = (Dictionary<string, object>)extractedItem["item"];
                item.ItemId = itemData["itemID"].ToString();
                item.SteamId = itemData["itemID"].ToString();
                item.Name = $"{itemData["displayFirstName"]} {itemData["displaySecondName"]}";
                var itemMarketplacePrice  = new ItemMarketplacePrice();
                itemMarketplacePrice.Price = double.Parse(itemData["itemPrice"].ToString());
                item.MarketplacePrice = itemMarketplacePrice;
                item.Rarity = itemData["rarity"].ToString();
                //
                var user = new User();
                var userData = (Dictionary<string, object>)extractedItem["user"];
                user.UserId = int.Parse(userData["id"].ToString());
                user.Name = userData["name"].ToString();
                var container = new Container();
                var caseId = extractedItem["caseID"].ToString();
                var casePrice = double.Parse(itemData["price"].ToString());
                container.Name = caseId;
                container.Price = casePrice;
                var date = new DateTime(1970, 1, 1).AddMilliseconds(long.Parse(extractedItem["date"].ToString()));
                res.Add(new ContainerExtractionData()
                {
                    Container = container,
                    DateRecive = date,
                    Item = item,
                    User = user
                });
            }

            return res.ToArray();
        }
    }
}

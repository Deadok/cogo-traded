﻿using System;
using System.Windows.Controls;
using CSGO.Trader.IntegratedPlugins.SteamMarketpace.UI;
using Trader.Plugin.PluginApi;

namespace CSGO.Trader.IntegratedPlugins.TopSkinMarketpace
{
    public class TopSkinMarketpaceSettings : ISettings
    {
        private TopSkinSettings _settingsControl; 

        public TopSkinMarketpaceSettings(MarketPlacePlugin plugin)
        {
            _settingsControl = new TopSkinSettings();
            _settingsControl.DataContext = this;
            Owner = plugin;
        }

        /// <summary>
        /// Плагин-владелец
        /// </summary>
        public MarketPlacePlugin Owner { get; private set; }

        public string Name => Owner.PluginName;

        /// <summary>
        /// Контролл настроек плагина стима
        /// </summary>
        public UserControl DisplayControl => _settingsControl;

        public void SaveSettings()
        {
            //throw new NotImplementedException();
        }

        public void ResetDefault()
        {
            //throw new NotImplementedException();
        }
    }
}

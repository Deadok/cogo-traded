﻿using System;
using System.Threading;
using SteamKit2;

namespace CSGO.Trader.IntegratedPlugins.SteamIntegrity
{
    public class SteamInteration
    {

        private bool _isRunning = true;

        /// <summary>
        /// Steam client
        /// </summary>
        private SteamClient _client;

        /// <summary>
        /// Callback manager
        /// </summary>
        private CallbackManager _callbackManager;

        /// <summary>
        /// Sync object
        /// </summary>
        private ManualResetEventSlim _mrs;

        /// <summary>
        /// Attached steam user
        /// </summary>
        private SteamUser _steamUser;

        /// <summary>
        /// Callback working thread
        /// </summary>
        private Thread _workingThread;

        /// <summary>
        /// 
        /// </summary>
        public SteamInteration()
        {
            _mrs = new ManualResetEventSlim();
            _client = new SteamClient();
            _callbackManager = new CallbackManager(_client);
            _steamUser = _client.GetHandler<SteamUser>();

            _workingThread = new Thread(() =>
            {
                while (_isRunning)
                {
                    _callbackManager.RunWaitAllCallbacks(TimeSpan.FromSeconds(1));
                }
            });
        }

        public void Inilialize()
        {
            //register callbacks
            _callbackManager.Subscribe<SteamClient.ConnectedCallback>(Connect);
            _callbackManager.Subscribe<SteamClient.DisconnectedCallback>(Disconnect);
            //
            _callbackManager.Subscribe<SteamUser.LoggedOnCallback>(LoggedOnCallback);
            _callbackManager.Subscribe<SteamUser.LoggedOffCallback>(LoggetOffCallback);
            _workingThread.Start();
        } 

        private void LoggetOffCallback(SteamUser.LoggedOffCallback loggedOffCallback)
        {
            throw new NotImplementedException();
        }

        private void LoggedOnCallback(SteamUser.LoggedOnCallback loggedOnCallback)
        {
            throw new NotImplementedException();
        }

        private void Disconnect(SteamClient.DisconnectedCallback disconnectedCallback)
        {
            throw new NotImplementedException();
        }

        private void Connect(SteamClient.ConnectedCallback connectedCallback)
        {
            throw new NotImplementedException();
        }
    }
}

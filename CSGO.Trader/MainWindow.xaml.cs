﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace CSGO.Trader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Closing += OnClosing;
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            StoreLayout();
        }

        private void StoreLayout()
        {
            LayoutManager.StoreLayout(DockingManager);
        }

        private void DockingManager_OnLoaded(object sender, RoutedEventArgs e)
        {
            LayoutManager.LoadLayout(DockingManager);
            ////Загрузить лейаут если есть
            //if (File.Exists(@".\AvalonDock.Layout.config"))
            //{
            //    var layoutSerializer = new XmlLayoutSerializer(this.DockingManager);
            //    using (var stream = new StreamReader(@".\AvalonDock.Layout.config"))
            //    {
            //        layoutSerializer.Deserialize(stream);
            //    }
            //}
        }
    }
}
